package Domain;

import java.beans.Transient;
import java.util.HashSet;
import java.util.Set;

public class Item {

    private static final String REFERENCE = "reference";
    private static final String SIZE = "size";
    private static final String PRICE = "price";
    private static final String COLOR = "color";
    private String price, ref, size;
    private Color color;
    private boolean hasError;

    /**
     * Error type can be either reference, size, price or color
     */
    private Set<String> errorTypes = new HashSet<>();


    public Item(String ref, String color, String price, String size) {

        // check if the input fields of this item contain errors
        this.size = (size == null) ? "" : size;
        this.ref = (ref == null) ? "" : ref;
        this.price = (price == null) ? "" : price;
        isValidInt(this.ref,REFERENCE);
        isWithTenNumbers(this.ref);
        isParsableFloat(this.price);
        isValidInt(this.size,SIZE);

        try {
            this.color =  Color.valueOf(color);
        }
        catch (IllegalArgumentException e) {
            this.color = Color.O;
            this.color.setProperty(color);
            this.hasError = true;
            errorTypes.add(COLOR);
        }
        catch (NullPointerException e) {
            this.color = Color.NOCOLOR;
            errorTypes.add(COLOR);
        }

    }

    /**
     * Method allonwing to check the format of the reference number (must have 10 digits)
     * @param
     */
    public void isWithTenNumbers(String s) {
         if(s.length() != 10)  {
             this.hasError = true;
             this.errorTypes.add(REFERENCE);
         }
    }


    public boolean isParsableInteger(String s) {
        int value;
        if (s == null || s.length() == 0 ) {
            return false;
        } else {
            try{
                value = Integer.valueOf(s);
                return true;
            } catch(NumberFormatException e) {
                this.hasError = true;
                return false;
            }
        }
    }
    /**
     * Method allowing to check if the String is parssable to a float
     * @param
     * @return
     */
    public void isParsableFloat(String s) {
        s = s.trim(); // sometimes user inputs white spaces without knowing it
        float value;
        if (s.length() == 0) {
            this.hasError = true;
            this.errorTypes.add(PRICE);
        } else {
            try{
                value = Float.valueOf(s);
            } catch(NumberFormatException e) {
                this.hasError = true;
                this.errorTypes.add(PRICE);
            }
        }
    }
    /**
     * Method allowing to check if the String is parssable to an Integer
     * @param
     * @return
     */
    public void isValidInt(String s, String field) {
        int number ;
        if(!isParsableInteger(s)) {
            number = 0;
            this.hasError = true;
            this.errorTypes.add(field);
        }
        else {
            number = Integer.parseInt(s);
        }
    }

    /**
     * Boolean indicating if this item contains error
     * @return
     */
    @Transient
    public boolean hasError() {
        return hasError;
    }

    @Transient
    public Set<String> getErrorTypes() {
        return errorTypes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }


    public String toString() {
        return this.ref+";"+this.color.getProperty()+";"+this.price+";"+this.size;
    }


}
