package Domain;

public enum Color {

    R("R"),
    G("V"),
    B("B"),
    O("Other"),
    NOCOLOR("");

    private String property;

    Color(String property) {
        this.property = property;
    }

    public String getProperty(){
        return this.property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
