import converter.Converter;
import converter.JsonConverter;
import converter.XmlConverter;

public class Main {

    public static void main(String[] args) {


        Converter jsonConverter = new JsonConverter();
        Converter xmlConverter = new XmlConverter();

        //Convert to json file
        jsonConverter.convert();
        //Convert to xml file
        xmlConverter.convert();
    }
}
