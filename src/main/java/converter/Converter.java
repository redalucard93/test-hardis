package converter;

import Domain.Error;
import Domain.Item;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class Converter {

    // Define the input file
    protected File csvFile = new File("items.txt");
    public static final String ERROR_MESSAGE = "Incorrect value for";
    public static final String ALL_FIELDS = "Reference, Price, Color, size";
    public static final String EMPTY_FIELDS = ";;;";


    /**
     * Convert a txt file to json file, and check if all the fields all correct
     * If one line contains an error, then we will have an object error in the returned json file
     * @param inputFile
     * @return
     */
    public Output convert(File inputFile) {

        // Eventual errors
        List<Error> errors = new ArrayList<>();

        // Pattern for splitting each line in the inputfil
        Pattern pattern = Pattern.compile(";");

        // Init field array
        String[] fields = new String[4];
        //
        Output output = null;

        //Read input file
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile))){
            // counter for lines
            int count = 1;

            // retrieve the list of all items
            List<Item> items = in
                    .lines()
                    .map(line -> {
                        for(int i=0; i<fields.length; i++) fields[i]=null;
                        String[] splittedLine = pattern.split(line);

                        //copy the splitted line tab to fields tab in order to avoid array out of boud exception
                        System.arraycopy(splittedLine, 0, fields, 0, splittedLine.length);
                        return new Item(fields[0],
                                fields[1],
                                fields[2],
                                fields[3]);


                    })
                    .collect(Collectors.toList());

            // use map to join each item to a line number
            Map<Item, Integer> itemWithLines = new LinkedHashMap<>();
            for(Item item : items) {
                itemWithLines.put(item,count);
                count++;
            }
            // if there is no item in the input file then we will get an error
            if(items.isEmpty()) {
                errors.add(new Error(0, ERROR_MESSAGE + " " + ALL_FIELDS, EMPTY_FIELDS));
            }

            // checking all errors while removing the wrong items
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {

                Item item = iterator.next();
                if(item.hasError()) {
                    // format error message
                    StringBuilder errorMessageBuilder = new StringBuilder();
                    item.getErrorTypes().forEach(message -> errorMessageBuilder.append(message+" ,"));
                    errorMessageBuilder.delete(errorMessageBuilder.length()-2,errorMessageBuilder.length());
                    Error error = new Error(itemWithLines.get(item), ERROR_MESSAGE + " " + errorMessageBuilder.toString() , item.toString());
                    // add new error to the list of errors
                    errors.add(error);
                    //remove the item
                    iterator.remove();
                }
            }
            // build the output json object and copy its content to an output file
            output = new Output(items,errors);
            buildFile(output);
        }
        catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return output;
    }

    public Output convert() {
        return convert(csvFile);
    }

    /**
     * Method building the file by passing the output object
     * The built file can either be in json or in xml
     * @param output
     */
    abstract public void buildFile(Output output);



}
