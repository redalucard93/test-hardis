package converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.*;


public class JsonConverter extends Converter {


    private File jsonFile = new File("items.json");

    public JsonConverter() {
        super();
    }

    public Output convert() {
        Output output = super.convert(csvFile);
        buildFile(output);
        return output;
    }

    /**
     * Build the json file using the result object
     * @param output
     */
    public void buildFile(Output output) {
        // Enable serialisation
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            // copy the content
            mapper.writeValue(jsonFile, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
