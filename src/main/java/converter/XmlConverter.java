package converter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;


public class XmlConverter extends Converter{

    // json output file
    private File xmlFile = new File("items.xml");

    public XmlConverter() {
        super();
    }

    public Output convert() {
        Output output = super.convert(csvFile);
        buildFile(output);
        return output;
    }

    /**
     * Build the xml file using the result object
     * @param output
     */
    public void buildFile(Output output) {

        JAXBContext jaxbContext;

        //Create JAXB Context
        try {
            jaxbContext = JAXBContext.newInstance(Output.class);

        //Create Marshaller
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        //Required formatting??
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        //Writes XML file to file-system
        jaxbMarshaller.marshal(output, this.xmlFile);


        } catch (JAXBException e) {
            System.err.println(e.getMessage());
        }


    }
}
