package converter;

import Domain.Error;
import Domain.Item;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

// specify that this instance of this class can be using in xml format
@XmlRootElement
@XmlType(propOrder={"items","errors"}) //ordering the elements
/**
 * the out out elements contains all the correct items and the errors found
 * while reading the input file
 */
public class Output {
    List<Item> items;
    List<Error> errors ;

    public Output() {
    }

    public Output(List<Item> items, List<Error> errors) {
        this.items = items;
        this.errors = errors;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

}
