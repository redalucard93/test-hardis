import converter.Converter;
import converter.JsonConverter;
import converter.Output;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

public class ConverterTestSize {

    private Converter converterTest;
    private File inputFile;
    @Before
    public void init() {
        this.converterTest = new JsonConverter();
        inputFile = new File("test.txt");
    }

    /**
     * Give valid size field
     */
    @Test
    public void validSizeTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1111111111;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 1);
            assertTrue(output.getErrors().size() == 0);

            assertTrue(output.getItems().get(0).getPrice().equals("45.12"));
            assertTrue(output.getItems().get(0).getRef().equals("1111111111"));
            assertTrue(output.getItems().get(0).getColor().getProperty().equals("R"));
            assertTrue(output.getItems().get(0).getSize().equals("27"));


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Give string size field
     */
    @Test
    public void stringSizeTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1111111111;R;45.12;a");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for size"));
            assertTrue(output.getErrors().get(0).getValue().equals("1111111111;R;45.12;a"));
            assertTrue(output.getErrors().get(0).getLine()== 1);


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }


    }

    /**
     * Give empty size field
     */
    @Test
    public void emptySize() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1111111111;R;45.12;");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for size"));
            assertTrue(output.getErrors().get(0).getValue().equals("1111111111;R;45.12;"));
            assertTrue(output.getErrors().get(0).getLine()== 1);


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }


    }

}
