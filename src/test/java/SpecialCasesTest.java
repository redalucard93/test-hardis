import converter.Converter;
import converter.JsonConverter;
import converter.Output;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

public class SpecialCasesTest {

    private Converter converterTest;
    private File inputFile;
    @Before
    public void init() {
        this.converterTest = new JsonConverter();
        inputFile = new File("test.txt");
    }
    /**
     * test with empty line
     */
    @Test
    public void emptyLineTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);
            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for Reference, Price, Color, size"));

        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    /*
     * Test with only one field
     */
    @Test
    public void onlyOneFieldTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1460100040");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);
            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for size ,color ,price"));
            assertTrue(output.getErrors().get(0).getValue().equals("1460100040;;;"));
            assertTrue(output.getErrors().get(0).getLine() == 1);
        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    /*
     * Test with multiple lines with empty first line
     */
    @Test
    public void emptyFirstLineTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("\n");
            myWriter.write("1460100040;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 1);
            assertTrue(output.getErrors().size() == 1);
            assertTrue(output.getErrors().get(0).getLine() == 1);
            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for reference ,size ,color ,price"));
            assertTrue(output.getErrors().get(0).getValue().equals(";;;"));
        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    /*
     * Test errors in two lines
     */
    @Test
    public void twoErrorsTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1460100040;R;a;27\n");
            myWriter.write("1460100040;A;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 2);


            assertTrue(output.getErrors().get(0).getLine() == 1);
            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for price"));
            assertTrue(output.getErrors().get(0).getValue().equals("1460100040;R;a;27"));

            assertTrue(output.getErrors().get(1).getLine() == 2);
            assertTrue(output.getErrors().get(1).getMessage().equals("Incorrect value for color"));
            assertTrue(output.getErrors().get(1).getValue().equals("1460100040;A;45.12;27"));

        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }


}
