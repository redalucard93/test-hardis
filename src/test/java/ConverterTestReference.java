import converter.Converter;
import converter.JsonConverter;
import converter.Output;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static junit.framework.TestCase.assertTrue;

public class ConverterTestReference {
    private Converter converterTest;
    private File inputFile;
    @Before
    public void init() {
        this.converterTest = new JsonConverter();
        inputFile = new File("test.txt");
    }

    /**
     * Give valid reference ID
     */
    @Test
    public void validReferenceTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("1111111111;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 1);
            assertTrue(output.getErrors().size() == 0);

            assertTrue(output.getItems().get(0).getRef().equals("1111111111"));



        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Give a string reference ID
     */
    @Test
    public void stringReferenceTest() {

        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("aaaaaaaaaa;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for reference"));
            assertTrue(output.getErrors().get(0).getValue().equals("aaaaaaaaaa;R;45.12;27"));
            assertTrue(output.getErrors().get(0).getLine()== 1);


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Give reference digits number > 10
     */
    @Test
    public void referenceFormatGtTenTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("11111111111;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for reference"));
            assertTrue(output.getErrors().get(0).getValue().equals("11111111111;R;45.12;27"));
            assertTrue(output.getErrors().get(0).getLine()== 1);


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }


    }

    /**
     * Give empty reference field
     */
    @Test
    public void emptyReferenceTest(){
        try{
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write(";R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for reference"));
            assertTrue(output.getErrors().get(0).getValue().equals(";R;45.12;27"));
            assertTrue(output.getErrors().get(0).getLine()== 1);

        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    /**
     * Give reference digits number > 10
     */
    @Test
    public void referenceFormatLtTenTest() {
        try {
            FileWriter myWriter = new FileWriter(this.inputFile);
            myWriter.write("11111111;R;45.12;27");
            myWriter.close();

            Output output = converterTest.convert(this.inputFile);

            assertTrue(output.getItems().size() == 0);
            assertTrue(output.getErrors().size() == 1);

            assertTrue(output.getErrors().get(0).getMessage().equals("Incorrect value for reference"));
            assertTrue(output.getErrors().get(0).getValue().equals("11111111;R;45.12;27"));
            assertTrue(output.getErrors().get(0).getLine()== 1);


        }  catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

}
