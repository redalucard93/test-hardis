# Converter

Converter is a Java tool for convert a txt file to json file before check all the errors 
that this file contains in each line. The errors are also shown in the out put file

## Installation
- Put the txt file in the root directory of the project
- Use Maven to build and install converter

```bash
mvn clean install
```

## Execution 
- Execute the main method in Main class


